package com.example.polegeaCats

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.marsphotos.R
import com.example.polegeaCats.ui.model.CatsUIModel
import com.example.polegeaCats.ui.screens.CatsViewModel
import com.example.polegeaCats.ui.theme.PolEgeaCats

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PolEgeaCats {
                CatsApp()
            }
        }
    }
}

@Composable
fun CatsApp(catsViewModel: CatsViewModel = viewModel()){
    val uiState by catsViewModel.uiState.collectAsState()
    CatList(catList = uiState)
}


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun CatList(catList: List<CatsUIModel>, modifier: Modifier = Modifier) {

    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(catList) { cat ->
            CatCard(catsUIModel = cat)

        }
    }
}

@Composable
fun CatCard(catsUIModel: CatsUIModel , modifier: Modifier = Modifier) {
    var expanded by remember {
        mutableStateOf(false)
    }

    Card(
        modifier = Modifier
            .padding(start = 8.dp, end = 8.dp, top = 5.dp, bottom = 5.dp)
            .fillMaxWidth()
            .height(if (expanded) 100.dp else 60.dp)
            .shadow(20.dp)
            .animateContentSize(
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioMediumBouncy,
                    stiffness = Spring.StiffnessLow
                )
            ),
        shape = RoundedCornerShape(20)

    ) {Column {
        Row(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.background(
                Color(MaterialTheme.colors.primary.red,MaterialTheme.colors.primary.green,MaterialTheme.colors.primary.blue,0.7f),
                shape = RoundedCornerShape(1)
            )) {

            Text(
                text = catsUIModel.name,
                color = MaterialTheme.colors.background,
                modifier = Modifier
                    .padding(start = 16.dp, end = 20.dp)
                    .height(60.dp)
                    .weight(1f),
                fontSize = 18.sp,
                textAlign = TextAlign.Left

            )
            Image(
                painter = rememberAsyncImagePainter(catsUIModel.img_url),
                contentDescription = "Imatge",
                modifier = Modifier
                    .clip(
                        RoundedCornerShape(100)
                    )
                    .height(60.dp),
                contentScale = ContentScale.Crop
            )
            IconButton(onClick = { expanded=!expanded }) {
                Icon(imageVector = if(!expanded)Icons.Filled.ArrowDropDown else Icons.Filled.ArrowDropUp,tint=MaterialTheme.colors.secondary, contentDescription = "icon")
            }
        }
        if(expanded)CatDesc(catsUIModel.temperament,catsUIModel)
    }
    }
}
@Composable
fun CatDesc(description: String,catsUIModel: CatsUIModel){
    val context= LocalContext.current
    Row(){
        Text(text = description, modifier = Modifier
            .padding(start = 10.dp, end = 10.dp)
            .width(269.dp))
        TextButton(onClick = {

            val inten= Intent(
                context,
                DetailActivity::class.java
            )
            inten.putExtra("name",catsUIModel.id)
            context.startActivity(inten)
        }) {
            Text(text = "See more")
        }
    }
}

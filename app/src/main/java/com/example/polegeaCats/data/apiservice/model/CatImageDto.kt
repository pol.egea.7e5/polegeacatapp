package com.example.polegeaCats.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CatImageDto(
    @SerialName("url") val imageUrl: String =""
)

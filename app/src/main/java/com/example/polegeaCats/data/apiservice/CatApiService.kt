package com.example.polegeaCats.data.apiservice

import com.example.polegeaCats.data.apiservice.model.CatImageDto
import com.example.polegeaCats.data.apiservice.model.CatsDto
import retrofit2.Retrofit
import retrofit2.http.GET
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.http.Query


private const val BASE_URL =
    "https://api.thecatapi.com/"

private val json = Json { this.ignoreUnknownKeys = true }

@OptIn(ExperimentalSerializationApi::class)
private val retrofit = Retrofit.Builder()
    .addConverterFactory(json.asConverterFactory(("application/json").toMediaType()))
    .baseUrl(BASE_URL)
    .build()

object CatsApi{
    val retrofitService : CatApiService by lazy{
        retrofit.create(CatApiService::class.java)
    }
}
interface CatApiService{
    @GET("./v1/breeds")
    suspend fun getCats(): List<CatsDto>
    @GET("./v1/images/search")
    suspend fun getCatImageRx(@Query("breed_id")id:String) : List<CatImageDto>
}
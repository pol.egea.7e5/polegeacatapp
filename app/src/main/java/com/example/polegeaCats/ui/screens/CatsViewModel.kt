package com.example.polegeaCats.ui.screens

import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.polegeaCats.data.apiservice.CatsApi
import com.example.polegeaCats.ui.model.CatsUIModel
import com.example.polegeaCats.ui.model.mapper.CatsDtoUIModelMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CatsViewModel : ViewModel() {
    private val _uiState = MutableStateFlow<List<CatsUIModel>>(emptyList())
    val uiState: StateFlow<List<CatsUIModel>> = _uiState.asStateFlow()

    private var mapper: CatsDtoUIModelMapper = CatsDtoUIModelMapper()
    init {
        viewModelScope.launch {
            val breeds = CatsApi.retrofitService.getCats()
            val photoListDto = breeds.flatMap { breed -> CatsApi.retrofitService.getCatImageRx(breed.id) }
            _uiState.value = mapper.map(breeds, photoListDto)
        }
    }
    fun getCat(id: String, xd:List<CatsUIModel>): CatsUIModel {
        return if(xd.find { x->x.id==id }!=null) xd.find { x->x.id==id }!!
        else CatsUIModel("","","","","","","")
    }
}
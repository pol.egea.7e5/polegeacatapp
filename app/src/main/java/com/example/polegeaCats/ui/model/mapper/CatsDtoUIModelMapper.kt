package com.example.polegeaCats.ui.model.mapper


import com.example.polegeaCats.data.apiservice.model.CatImageDto
import com.example.polegeaCats.data.apiservice.model.CatsDto
import com.example.polegeaCats.ui.model.CatsUIModel


class CatsDtoUIModelMapper {
    fun map(breeds: List<CatsDto>, images: List<CatImageDto>): List<CatsUIModel> {
        return images.mapIndexed { index, image ->
            CatsUIModel(
                breeds[index].id,
                image.imageUrl,
                breeds[index].name,
                breeds[index].temperament,
                breeds[index].countryCode,
                breeds[index].description,
                breeds[index].wikipedia_url
            )
        }
    }
}
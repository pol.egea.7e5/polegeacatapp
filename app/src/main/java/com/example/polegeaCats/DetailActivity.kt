package com.example.polegeaCats

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.polegeaCats.ui.model.CatsUIModel
import com.example.polegeaCats.ui.screens.CatsViewModel
import com.example.polegeaCats.ui.theme.PolEgeaCats


var id:String=""
class DetailActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?)
    {
        id=intent.extras?.getString("name").toString()
        super.onCreate(savedInstanceState)
        setContent {
            PolEgeaCats {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                        MyApp()
                }
            }
        }
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MyApp(catsViewModel: CatsViewModel = viewModel()) {
    val uiState by catsViewModel.uiState.collectAsState()
    var catsUIModel:CatsUIModel=catsViewModel.getCat(id,uiState)
    Scaffold(
        content = {
            Greeting(
                name = catsUIModel.name,
                description = catsUIModel.description+"\n\n"+ catsUIModel.temperament+"\n\n"+ catsUIModel.countryCode+"\n\n"+ catsUIModel.wikipedia_url,
                catsUIModel = catsUIModel
            )
        }
    )
}

@Composable
fun Greeting(name: String, description: String,catsUIModel: CatsUIModel) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .background(
                Brush.verticalGradient(
                    colors = listOf(
                        Color(MaterialTheme.colors.primary.red,MaterialTheme.colors.primary.green,MaterialTheme.colors.primary.blue,0.7f),
                        LocalContentColor.current.copy(LocalContentAlpha.current),
                        Color(MaterialTheme.colors.primary.red,MaterialTheme.colors.primary.green,MaterialTheme.colors.primary.blue,0.7f)
                    )
                ), shape = RoundedCornerShape(5)
            ),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.padding(top = 16.dp))

        Image(
            modifier = Modifier
                .clip(shape = /*CutCornerShape(20.dp)*/RoundedCornerShape(percent = 100))
                .shadow(100.dp)
                .size(180.dp),
            painter = rememberAsyncImagePainter(catsUIModel.img_url),
            contentDescription = "DjHuevo",
            contentScale = ContentScale.Crop,
        )
        Spacer(modifier = Modifier.padding(top = 16.dp))
        Text(
            text = name.uppercase(),
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(top = 8.dp, bottom = 8.dp),
            fontSize = 50.sp,
            maxLines = 1,
            color= MaterialTheme.colors.background
        )
        Text(
            text = description,
            modifier = Modifier.padding(top = 8.dp, start = 8.dp, end = 8.dp),
            fontSize = 20.sp,
            textAlign = TextAlign.Justify,
            color= MaterialTheme.colors.background
        )

    }

}